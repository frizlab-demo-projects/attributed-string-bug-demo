/*
 * AttributedStringBugDemoTest.swift
 * AttributedStringBugDemoTest
 *
 * Created by François Lamboley on 2022/11/09.
 */

import AppKit
import Foundation
import XCTest



final class AttributedStringBugDemoTest : XCTestCase {
	
	override func setUpWithError() throws {
	}
	
	override func tearDownWithError() throws {
	}
	
	func testSameThingAsInExecutableButInATestCase() throws {
		let baseAttributes = {
			var res = AttributeContainer()
			res.font = .systemFont(ofSize: 14)
			res.foregroundColor = .black
			return res
		}()
		
		var attrStr = AttributedString("yolo^+1 result<:s>^", attributes: baseAttributes)
		attrStr.replaceSubrange(attrStr.range(of: "<:s>")!, with: AttributedString(""))
		attrStr[attrStr.range(of: "1 result")!].font = .preferredFont(forTextStyle: .caption1)
		
		let range = attrStr.range(of: "+1 result")!
		XCTAssertEqual(String(describing: attrStr[range]), String(describing: AttributedString(attrStr[range])))
	}
	
}
