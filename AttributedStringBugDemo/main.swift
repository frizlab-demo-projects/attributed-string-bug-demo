/*
 * main.swift
 * AttributedStringBugDemo
 *
 * Created by François Lamboley on 2022/11/09.
 */

import AppKit
import Foundation



do {
	let baseAttributes = {
		var res = AttributeContainer()
		res.font = .systemFont(ofSize: 14)
		res.foregroundColor = .black
		return res
	}()
	
	var attrStr = AttributedString("yolo^+1 result<:s>^", attributes: baseAttributes)
	attrStr.replaceSubrange(attrStr.range(of: "<:s>")!, with: AttributedString(""))
	attrStr[attrStr.range(of: "1 result")!].font = .preferredFont(forTextStyle: .caption1)
	
	let range = attrStr.range(of: "+1 result")!
	print("SUBATTRSTR")
	print(attrStr[range])
	print("ATTRSTR from sub")
	print(AttributedString(attrStr[range]))
}
